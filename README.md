resume
======

My resume, created in Latex.

To create a similar file, you need to put res.cls (a template for resume generator) in the same folder as resume.tex. Then make changes to the tex file using a text (or Tex) editor (I highly recommend Texmaker). Finally, make sure you have a tex to pdf converted installed (say pdflatex), then on the terminal, just run

	pdflatex resume.tex
	
...And your beautifully created resume in pdf format appears in the same directory. D: